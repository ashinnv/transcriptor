#!/usr/bin/bash

echo "setting up dependencies"
sudo apt install python3 python3-pip fish

echo "starting build process"

git clone "https://gitlab.com/ashinnv/transcriptor"
cd transcriptor
python3 -m venv transcriptor
source transcriptor/bin/activate
pip install unidecode mysql.connector feedparser requests git+https://github.com/openai/whisper.git ffmpeg-python 

echo "Building database."

mysql -u root -p <<EOF

# Create the database
CREATE DATABASE IF NOT EXISTS podcast_transcripts;

# Use the database
USE podcast_transcripts;

# Create the table
CREATE TABLE IF NOT EXISTS episodes (
    episode_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    show_name TEXT,
    episode_title TEXT,
    transcript LONGTEXT,
    episode_release_timestamp BIGINT(20),
    transcription_model VARCHAR(128),
    transcription_date BIGINT(20) UNSIGNED,
    PRIMARY KEY (episode_id)
);

EOF

echo "ready to run"