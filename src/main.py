from concurrent.futures import ThreadPoolExecutor
from unidecode import unidecode
import concurrent.futures
import mysql.connector
import feedparser
import threading
import requests
import whisper
import random
import string
import ffmpeg
import time
import json
import os

global tit
tit = ""

global mod
mod = ""

global modsource
modsource = ""

#Using variable declaration here to make it easier to add functionality in the future
def clean_text(input):
   cleaned = unidecode(input)
   return cleaned

def download_target(tname, obj):
  print("Starting download target")
  resp = requests.get(obj['link'])
  if resp.status_code == 200:
    file_data = resp.content

    input_audio = ffmpeg.input('pipe:0', format=obj['type'])
    output_audio = ffmpeg.output(input_audio, 'pipe:1', format='wav')
    out, _ = ffmpeg.run(output_audio, input=file_data, capture_stdout=True, capture_stderr=True)
    
    try:
      with open(tname, 'wb') as f:
        f.write(out)
    except:
      print("Failed to write tmp file.")
    #return out

def genstring():
    letters = string.ascii_letters
    name = ''.join(random.choice(letters) for _ in range(45))
    print("generated " + name + ".mp3")
    return name

def parse_links(input_entry):
  for link in input_entry:
    #print("LT:",link.type)
    if link.type == "audio/mpeg":
      return link.href, "mp3"
    elif link.type == "audio/m4a":
      return link.href, "m4a"
    elif link.type == "audio/x-m4a":
      return link.href, "m4a"

  #time.sleep(100000)
def get_audio_links(show_link):
  entries = []
  feed = feedparser.parse(show_link)
  global tit 
  try:
    tit = clean_text(feed['feed']['title'])
  except Exception as e:
     print("Error with ct: ", e)
     quit()

  for entry in feed.entries:
    this_episode = {}
    this_episode['episode_title'] = clean_text(entry['title'])
    if 'links' in entry:
      #for elem in entry:
        #print(elem)
      #print("Links content: ", entry.links[1].type)
      linkref, linktype = parse_links(entry.links)
      #print("FOUND: ", linkref, linktype)
      this_episode['type'] = linktype
      this_episode['link'] = linkref
    else:
      print("COULDN'T FIND LINKS IN FEED")

    #print("Appending to entries.")
    #Sprint(this_episode)
    entries.append(this_episode)
    #time.sleep(1)

  #print("returning entries")
  return feed['feed']['title'],entries

def delete_tmp_file(file_name):
    # Check if the file exists
    if os.path.exists(file_name):
        # Delete the file
        os.remove(file_name)
        #print(f"The file {file_name} has been deleted.")
    else:
        print("The file does not exist.")


def run_transcription(target):
  tmp_name = genstring()+"."+target['type']

  global mod
  global modsource 

  modsource = "openai"

  #mod = "tiny"
  #mod = "base"
  #mod = "small"
  mod = "medium"
  #mod = "large"
  #mod = "large-v2"
 
  print("Using -- ", modsource,":", mod)

  model = whisper.load_model(mod)
  download_target(tmp_name, target)
  res = model.transcribe(tmp_name)
  delete_tmp_file(tmp_name)
  return res['text']

def check_db_for_title(show_name, title):
    
    global tit
    # Create a connection to the database
    cnx = mysql.connector.connect(user='root', password='313231a',
                                  host='localhost',
                                  database='podcast_transcripts')

    # Create a cursor object
    cursor = cnx.cursor()

    # Define the search query
    search_query = ("SELECT * FROM episodes WHERE BINARY show_name = %s AND BINARY episode_title = %s")
    #print("SQ: ", search_query)
    
    # Execute the search query
    cursor.execute(search_query, (tit, title))

    # Fetch the results
    result = cursor.fetchone()

    # Fetch all remaining rows to avoid the 'Unread result found' error
    while cursor.fetchone() is not None:
        pass

    cursor.close()
    cnx.close()

    # Return False if a match is found, True otherwise
    return result is None

"""
def check_db_for_title(show_name, title):
    
    global tit
    # Create a connection to the database
    cnx = mysql.connector.connect(user='root', password='313231a',
                                  host='localhost',
                                  database='podcast_transcripts')

    # Create a cursor object
    cursor = cnx.cursor()

    # Define the search query
    search_query = ("SELECT * FROM episodes WHERE show_name = %s AND episode_title = %s")
    print("SQ: ", search_query)
    
    # Execute the search query
    cursor.execute(search_query, (tit, title))

    # Fetch the results
    result = cursor.fetchone()

    # Fetch all remaining rows to avoid the 'Unread result found' error
    while cursor.fetchone() is not None:
        pass

    cursor.close()
    cnx.close()

    # Return False if a match is found, True otherwise
    return result is None

"""
def write_to_db(show):
    
    global mod
    global modsource

    timestamp = time.time_ns()
    #print("Writing show_name variable: ", show)
    
    # Create a connection to the database
    try:
      cnx = mysql.connector.connect(user='root', password='313231a',
                                  host='localhost',
                                  database='podcast_transcripts')
    except:
      print("Failed to get a connection.")
    # Create a cursor object
    cursor = cnx.cursor()

    # Define the insert query
    try:
      add_show = ("INSERT INTO episodes "
                "(show_name, episode_title, transcript, episode_release_timestamp, transcription_model, transcription_date) "
                "VALUES (%s, %s, %s, %s, %s, %s)")

      # Insert new show
      cursor.execute(add_show, (tit, show['episode_title'], show['transcript'], 0, modsource+":"+mod,timestamp)) #show['episode_release_timestamp']))

      # Make sure data is committed to the database
      cnx.commit()
    except Exception as e:
      print("Failed to commit due to: ", e)
      exit()

    cursor.close()
    cnx.close()


def process_link(key, show_name):
    #loop_count=0 #Fix this later.
    #print("=======V Loop count:", loop_count, "V======")

    if not check_db_for_title(tit,key['episode_title']):
      #print("\n\n\n\n")
      #print("Already found:", key['episode_title'])
      #print("\n\n\n\n")
      return
    else:
      print()
      #print("\n\n\n\n")
      #print("title not found:", key['episode_title'])
      #3print("\n\n\n\n")

    #print("Show name short: {n}",tit)
    trans = run_transcription(key)
    key['transcript'] = trans
    write_to_db(key)
    return key









#TODO: Place main here==================================================================================


#targ = 'http://rss.sciam.com/sciam/60secsciencepodcast'
targ = 'https://anchor.fm/s/69e136c4/podcast/rss'
_, links = get_audio_links(targ)
print("Links length: ", len(links))
#time.sleep(100)
num_threads = 1

# Create a ThreadPoolExecutor
#with concurrent.futures.ThreadPoolExecutor(max_workers=num_threads) as executor:
#    results = list(executor.map(process_link, links, tit))
for link in links:
   process_link(link, tit)
